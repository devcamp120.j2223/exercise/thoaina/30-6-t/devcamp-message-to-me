
import image from "../../../assets/images/b68a5d6aac9d02151f721e2427c7fe79.jpg";
import { Row, Col } from "reactstrap";

function TitleImage() {
    return (
        <Row>
            <Col>
                <img src={image} width="500px" className="img-thumbnail" />
            </Col>
        </Row>
    )
}

export default TitleImage;