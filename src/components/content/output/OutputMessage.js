
import likeImg from "../../../assets/images/1196px-Facebook_like_thumb.png";

function OutputMessage({ messageOutProp, likeImageProp }) {
    return (
        <div>
            <div className="row mt-3">
                <div className="col-12">
                    {
                        messageOutProp.map((value, index) => {
                            return <p key={index}>{value}</p>
                        })
                    }
                </div>
            </div>
            {
                likeImageProp ?
                    <div className="row mt-3">
                        <div className="col-12">
                            <img src={likeImg} width="50px" className="img-thumbnail" />
                        </div>
                    </div> : null
            }
        </div>
    )
}

export default OutputMessage;