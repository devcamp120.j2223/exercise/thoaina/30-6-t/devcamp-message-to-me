
import { useState } from "react";
import InputMessage from "./input/InputMessage";
import OutputMessage from "./output/OutputMessage";

function ContentComponent() {
    const [messageInput, setMessageInput] = useState("Xin mời nhập message...");
    const [messageOutput, setMessageOutput] = useState([]);
    const [likeImage, setLikeImage] = useState([]);

    const changeInputMessageHandler = (value) => {
        console.log(value);
        setMessageInput(value);
    }

    const changeOutMessageHandler = (value) => {
        setMessageOutput([...messageOutput, messageOutput]);
        setLikeImage(messageInput ? true : false);
    }

    return (
        <div>
            <InputMessage messageInputProp={messageInput} changeInputMessageProp={changeInputMessageHandler} changeOutMessageProp={changeOutMessageHandler} />
            <OutputMessage messageOutProp={messageOutput} likeImageProp={likeImage} />
        </div>
    )
}

export default ContentComponent;