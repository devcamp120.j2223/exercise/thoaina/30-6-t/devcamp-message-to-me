
import { Button, Col, Input, Label, Row } from "reactstrap";

function InputMessage({ messageInputProp, changeInputMessageProp, changeOutMessageProp }) {
    const inputChangeHandler = (event) => {
        console.log("Giá trị input thay đổi");
        console.log(event.target.value);
        changeInputMessageProp(event.target.value);
    }

    const buttonClickHandler = () => {
        console.log("Nút được bấm");
        changeOutMessageProp();
    }

    return (
        <div>
            <Row>
                <Col className="mt-3">
                    <Label className="form-label">Message cho bạn 12 tháng tới:</Label>
                </Col>
            </Row>
            <Row>
                <Col className="mt-3">
                    <Input className="form-control" placeholder="Nhập message" onChange={inputChangeHandler} />
                </Col>
            </Row>
            <Row>
                <Col className="mt-3">
                    <Button color="success" onClick={buttonClickHandler}>Gửi thông điệp</Button>
                </Col>
            </Row>
        </div>
    )
}

export default InputMessage;